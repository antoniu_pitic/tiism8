#include "MAtrici.h"

void AfisareMatrice(int A[10][10], int n, int m)
{
	for (int i = 0; i < n; i++) {
		for (int j = 0; j < m; j++) {
			cout.width(3);
			cout << A[i][j];
		}
		cout << endl;
	}
	cout << endl;
}

void AfisareMatriceStangaDreapta(int A[10][10], int n, int m)
{
	for (int i = 0; i < n; i++) {
		if (i % 2 == 0) {
			for (int j = 0; j < m; j++) {
				cout.width(3);
				cout << A[i][j];
			}
		}
		else {
			for (int j = m-1; j >=0; j--) {
				cout.width(3);
				cout << A[i][j];
			}

		}
	}
	cout << endl;
}

void GenerareMatriceCrescator(int A[10][10], int & n, int & m)
{
	int x = 1;
	for (int i = 0; i < n; i++) {
		for (int j = 0; j < m; j++) {
			A[i][j] = x++;
		}
	}
}
/*
00 01 02 03 04
10 11 12 13 14
20 21 22 23 24
30 31 32 33 34
40 41 42 43 44
*/
void Melcu(int A[10][10], int n)
{
	for (int k = 0; k <= n / 2; k++) {
		for (int j = k; j < n - k; j++) {
			cout << A[k][j] << " ";
		}

		for (int i = k + 1; i < n - k; i++) {
			cout << A[i][n - 1 - k] << " ";
		}

		for (int j = n - 2 - k; j >= k; j--) {
			cout << A[n - 1 - k][j] << " ";
		}
		for (int i = n - 2 - k; i >= k + 1; i--) {
			cout << A[i][k] << " ";
		}
	}
}


/*
int S=0;
for (int i = 0; i < n; i++) {
	for (int j = 0; j < m; j++) {
		S += A[i][j];
	}
}

*/